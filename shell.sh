#!/bin/sh

echo "Main program is running"
cat /dev/null > output.c
echo '\n' "Please enter the input filename : " 
read ipFilename

while true; do

echo '\n' "Please choose any of the below option " '\n' "A for Mean Median Mode " '\n' "B for Range, InterQuarter Range and Standard Deviation " '\n' "G for Graphs in libreoffice "'\n' "X for exit "
read readOption

if  ( [ "$readOption" = "A" ] || [ "$readOption" = "B" ] )
then
	echo -n "Valid entries "
fi

case "$readOption" in
  A)
    echo "Calling m"
    ./m $ipFilename
    ;;
  B)
    echo "Calling range"
    ./range $ipFilename
    ;;
  G) 
   echo "Calling libreoffice "
   libreoffice --calc /home/mtech/Desktop/r/mgraph.ods
    ;;
  X)
    echo '\n' "Good Bye "
    break
    ;;
  *)
	echo '\n' "Invalid entries, please try again "
    ;;
esac

#if ( [ "$readOption" <> "X" ] && [ "$readOption" <> "A" ] && [ "$readOption" <> "B" ] )
#then
#	echo '\n' "Invalid entries, please try again "
#fi
done
